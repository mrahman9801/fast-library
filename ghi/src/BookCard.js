import { useBorrowBookMutation, useReturnBookMutation } from './app/api';

function BookCardButton(props) {
  const { book, isPatron, accountId } = props;
  const [ borrowBook, { error: borrowError } ] = useBorrowBookMutation();
  const [ returnBook, { error: returnError } ] = useReturnBookMutation();
  const loans = book.loans;

  if (!loans) {
    return null;
  } else if (borrowError || returnError) {
    return (
      <div className="card-header">
        <button className="button is-danger disabled is-fullwidth" disabled>Refresh the page</button>
      </div>
    )
  } else if (isPatron && loans.includes(accountId)) {
    return (
      <div className="card-header">
        <button onClick={() => returnBook(book.id)} className="button is-warning is-radiusless is-fullwidth">
          Return
        </button>
      </div>
    )
  } else if (isPatron && book.quantity - loans.length <= 0) {
    return (
      <div className="card-header">
        <button className="button disabled is-fullwidth" disabled>Not available</button>
      </div>
    )
  } else if (isPatron) {
    return (
      <div className="card-header">
        <button onClick={() => borrowBook(book.id)} className="button is-radiusless is-info is-fullwidth">
          Borrow
        </button>
      </div>
    );
  }
}

function BookCard(props) {
  const { book, isPatron, accountId } = props;

  return (
    <div className="card is-flex is-flex-direction-column">
      <BookCardButton book={book} isPatron={isPatron} accountId={accountId} />
      <div className="card-header">
        <p className="is-size-7 is-justify-content-center card-header-title">
          { book.quantity - (book.loans || []).length } of { book.quantity } copies available
        </p>
      </div>
      { book.cover
      ? <div className="card-image">
          <figure className="image is-3by4">
            <img src={book.cover} alt={book.title} />
          </figure>
        </div>
      : null }
      <div className="card-content is-flex-grow-1">
        <div className="media">
          <div className="media-content">
            <p className="title is-4">{ book.title }</p>
            <p className="subtitle is-6">
              by <b>{ book.author }</b>
            </p>
          </div>
        </div>
        <p>{ book.synopsis }</p>
      </div>
    </div>
  );
}

export default BookCard;
