import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routers import loans
from routers import sockets

app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", "http://localhost"),
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(loans.router, prefix="/api")
app.include_router(sockets.router)
