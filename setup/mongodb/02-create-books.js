conn = Mongo();
db = conn.getDB('library');

// Initialize starter books for library

db.inventory.insertMany([
  {
    _id: ObjectId('6331860de34b7947e69e7626'),
    author: 'Bradbury, Ray',
    title: 'Fahrenheit 451',
    quantity: 8,
    synopsis: 'Often regarded as one of Bradbury\'s greatest works, Fahrenheit 451 presents an American society where books have been outlawed and "firemen" burn any that are found.',
    cover: 'https://upload.wikimedia.org/wikipedia/en/d/db/Fahrenheit_451_1st_ed_cover.jpg'
  },
  {
    _id: ObjectId('633186c8e34b7947e69e7627'),
    author: 'Heinlein, Robert A.',
    title: 'Have Space Suit - Will Travel',
    quantity: 2,
    synopsis: 'High school senior Clifford "Kip" Russell is determined to get to the Moon, but the price of a ticket is far beyond his reach. He enters a contest wins a used space suit which he names "Oscar". A flying saucer gets Kip and hijinks ensue.',
    cover: 'https://upload.wikimedia.org/wikipedia/en/1/11/Have_Space_suit.jpg'
  },
  {
    _id: ObjectId('6331871be34b7947e69e7628'),
    author: 'Jemisin, N. K.',
    title: 'Fifth Season, The',
    quantity: 3,
    synopsis: 'The Fifth Season takes place on a planet with a single supercontinent called the Stillness. Every few centuries, its inhabitants endure what they call a "Fifth Season" of catastrophic climate change.',
    cover: 'https://upload.wikimedia.org/wikipedia/en/0/01/The_Fifth_Season_%28novel%29.jpg'
  },
  {
    _id: ObjectId('633187f2e34b7947e69e7629'),
    author: 'North, Claire',
    title: 'First Fifteen Lives of Harry August, The',
    quantity: 7,
    synopsis: 'Harry August is born in the women\'s washroom of Berwick-upon-Tweed station in 1919, leads an unremarkable life, and dies in hospital in Newcastle-upon-Tyne in 1989. He then finds himself born again back in 1919 in the same circumstances.',
    cover: 'https://upload.wikimedia.org/wikipedia/en/b/b8/TheFirstFifteenLivesOfHarryAugust.jpg'
  },
  {
    _id: ObjectId('633188cce34b7947e69e762a'),
    author: 'Walschots, Natalie Zina',
    title: 'Hench',
    quantity: 1,
    synopsis: 'Anna Tromedlov works in the gig economy, providing clerical services to low-level supervillains in need of "henches" — until she is injured and disabled by a superhero\'s collateral damage.',
    cover: 'https://images.booksense.com/images/585/978/9780062978585.jpg'
  },
  {
    _id: ObjectId('63318954e34b7947e69e762b'),
    author: 'Glass, Jenna',
    title: 'Women\'s War, The',
    quantity: 4,
    synopsis: 'In a feminist fantasy epic, a revolutionary spell gives women the ability to control their own fertility—with consequences that rock their patriarchal society to its core.',
    cover: 'https://www.jennaglass.com/wp-content/uploads/2019/11/womens-war541-200x300.jpg'
  },
  {
    _id: ObjectId('633189e3e34b7947e69e762c'),
    author: 'Sweeney-Baird, Christina',
    title: 'End Of Men, The',
    quantity: 3,
    synopsis: 'Set in a world where a virus stalks our male population, The End of Men is an electrifying and unforgettable debut from a remarkable new talent that asks: what would life truly look like without men?',
    cover: 'https://dynamic.indigoimages.ca/books/0385696639.jpg?scaleup=true&width=614&maxheight=614&quality=85&lang=en'
  },
  {
    _id: ObjectId('63318c6ce34b7947e69e762d'),
    author: 'Chambers, Becky',
    title: 'Psalm for the Wild-Built, A',
    quantity: 22,
    synopsis: 'On a moon called Panga where AI and robots are a distant myth, Dex is an adventurous and friendly tea monk who travels the human-populated areas of their moon meeting villagers and townsfolk. One day Dex, seeking a change in their routine, travels into the wild and meets a robot named Splendid Speckled Mosscap. They are thrown into a road-trip with a question on their minds: "What do people need?"',
    cover: 'https://upload.wikimedia.org/wikipedia/en/a/a5/PsalmforWild.jpg'
  },
]);
