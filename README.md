# Fast Library

This is an online library built with FastAPI (my favorite framework), MongoDB, and React. Built to learn MongoDB integration and asynchronous UI updates with WebSockets.

## Screenshots

### Home Page

![Home Page](images/MainPage.jpg)

### Login

![Login](images/Login.gif)

### WebSocket Demonstration

![WebSocket Demon](images/WebSocketDemo.gif)

### Adding Books (only available to Librarian role)

![Adding Books](images/AddingBook.jpg)

## Running the application

1. Clone this repo.
1. Change your working directory to the cloned repo's
   directory.
1. Run the following commands:

    ```sh
    docker volume create node-modules
    docker volume create mongo-data
    docker compose build
    docker compose up
    ```

## View OpenAPI documentation

Access the docs at:

* Accounts API: <http://localhost:8000/docs>!
* Books API: <http://localhost:8001/docs>!

## Logging in

Access the GHI at <http://localhost:3000>.

The preloaded data contains two logins to use with
different permissions.

| Email                   | Password   |
|-------------------------|------------|
| `librarian@example.com` | `password` |
| `patron@example.com`    | `password` |

## Playing with WebSockets

Open the application in a normal browser
window, and open it again on an incognito tab.
Navigate to the interface at <http://localhost:3000>.

Be logged into an account for at least one window.
When you open or close a loan, the number of available copies
update for the other user.

## Seeing the data

You can see the data in MongoDB in the `library` database
and access it at <http://localhost:8081/db/library/>.
